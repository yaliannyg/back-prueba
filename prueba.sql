-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 19-07-2020 a las 02:54:01
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.2.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `prueba`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sequelizemeta`
--

CREATE TABLE `sequelizemeta` (
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tickets`
--

CREATE TABLE `tickets` (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `id_user` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `ticket_pedido` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tickets`
--

INSERT INTO `tickets` (`id`, `id_user`, `ticket_pedido`, `created_at`) VALUES
('48e27c92-498e-4dc6-b7d1-941c75f59def', '25d9b585-5ac0-4fdc-993a-a411bb05a164', 0, '2020-07-19 00:28:44'),
('5b83e9c3-a23b-409b-8307-21d296fb5b4b', '8584c71b-d245-43eb-8aba-51c3e9aa9f71', 1, '2020-07-19 00:32:35'),
('bcf9d7dc-10b5-409a-96d3-32d75c10d9c7', '8584c71b-d245-43eb-8aba-51c3e9aa9f71', 1, '2020-07-19 00:33:49'),
('d25bd1da-0e95-4854-818b-52a10e527889', '8584c71b-d245-43eb-8aba-51c3e9aa9f71', 1, '2020-07-19 00:29:03'),
('fccd82b4-011d-4138-a73f-2ffbcee44910', '8584c71b-d245-43eb-8aba-51c3e9aa9f71', 0, '2020-07-19 00:28:47');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_usuarios`
--

CREATE TABLE `tipo_usuarios` (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `nombre` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tipo_usuarios`
--

INSERT INTO `tipo_usuarios` (`id`, `nombre`) VALUES
('0f091598-b0f2-4e59-bbca-01cf334c0275', 'User'),
('120e31a7-145c-46ea-a417-03b81229c4b4', 'Admin');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `mail` varchar(255) DEFAULT NULL,
  `pass` varchar(255) DEFAULT NULL,
  `id_tipouser` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `mail`, `pass`, `id_tipouser`) VALUES
('25d9b585-5ac0-4fdc-993a-a411bb05a164', 'gabriel', 'luluhimym@gmail.com', '$2b$10$CyDTRyUnXRFtoj5s5hGfYOwqFys72iLwinldmYMQV2OkzV/x.gUui', '0f091598-b0f2-4e59-bbca-01cf334c0275'),
('8584c71b-d245-43eb-8aba-51c3e9aa9f71', 'julian gonzalez', 'yangonz1512@gmail.com', '$2b$10$LJE04R6vwZF0ZlRAM6/V2ead9pi50M06hxOaKi/SXlfNQmbr6xXtW', '0f091598-b0f2-4e59-bbca-01cf334c0275'),
('8e4bfb3b-5106-49e7-bbe9-962792457fde', 'yalianny gonzalez', 'yaliannysg@gmail.com', '$2b$10$UVPS6YRVp51Hdz20z/Y5f.fmWcfLwiF4DvSub154MnoXWKguPSrQ6', '120e31a7-145c-46ea-a417-03b81229c4b4');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `sequelizemeta`
--
ALTER TABLE `sequelizemeta`
  ADD PRIMARY KEY (`name`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indices de la tabla `tickets`
--
ALTER TABLE `tickets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tickets_id_user_usuarios_fk` (`id_user`);

--
-- Indices de la tabla `tipo_usuarios`
--
ALTER TABLE `tipo_usuarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `usuarios_id_tipouser_tipo_usuarios_fk` (`id_tipouser`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `tickets`
--
ALTER TABLE `tickets`
  ADD CONSTRAINT `tickets_id_user_usuarios_fk` FOREIGN KEY (`id_user`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `usuarios_id_tipouser_tipo_usuarios_fk` FOREIGN KEY (`id_tipouser`) REFERENCES `tipo_usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

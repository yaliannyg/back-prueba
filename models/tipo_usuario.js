const { Sequelize, DataTypes, Model } = require("sequelize");
// const sequelize = new Sequelize("sqlite::memory");
const db =require('./index');
const sequelize = db.sequelize

class Tipo_usuario extends Model {}

Tipo_usuario.init(
  {
    // Model attributes are defined here
    id: {
      type: Sequelize.UUID,
			primaryKey: true,
			unique: true,
      allowNull: false,
    },
    nombre: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  },
  {
    sequelize, // We need to pass the connection instance
    timestamps: false,
    freezeTableName: true,
    tableName: "tipo_usuarios",
    modelName: "tipo_usuario", // We need to choose the model name
    classMethods: {},
  }
);

module.exports = Tipo_usuario;
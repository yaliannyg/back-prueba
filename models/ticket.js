const { Sequelize, DataTypes, Model } = require("sequelize");
// const sequelize = new Sequelize("sqlite::memory");
const db =require('./index');
const sequelize = db.sequelize
const User = require('./usuario')

class Ticket extends Model {}

Ticket.init(
  {
    // Model attributes are defined here
    id: {
      type: Sequelize.UUID,
			primaryKey: true,
			unique: true,
      allowNull: false,
    },
    id_user: {
      type: DataTypes.STRING,
      allowNull: true,
      unique: true,
    },
    ticket_pedido: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      unique: true,
    },
    createdAt: {
      type: DataTypes.DATE,
      // defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
      timestamps: true,
      field: "created_at"
    }
  },
  {
    sequelize, // We need to pass the connection instance
    timestamps: false,
    freezeTableName: true,
    tableName: "tickets",
    modelName: "tickets", // We need to choose the model name
    classMethods: {},
  }
);

Ticket.id_user = Ticket.belongsTo(User, {foreignKey: 'id_user', target_key: 'id'})
module.exports = Ticket;
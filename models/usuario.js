const { Sequelize, DataTypes, Model } = require("sequelize");
const Tipo_usuario = require('./tipo_usuario')
// const {db} =require('./index');
const db =require('./index');
const sequelize = db.sequelize


class User extends Model {}

User.init(
  {
    // Model attributes are defined here
    // id: {
    //   type: Sequelize.UUID,
		// 	primaryKey: true,
		// 	unique: true,
    //   allowNull: false,
    // },
    nombre: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    mail: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    pass: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    id_tipouser: {
      allowNull: false,
      type: Sequelize.UUID,
      references: {
        model: "tipo_usuarios",
        key: "id",
      },
    },
  },
  {
    sequelize, // We need to pass the connection instance
    timestamps: false,
    freezeTableName: true,
    tableName: "usuarios",
    modelName: "usuario", // We need to choose the model name
    classMethods: {},
  }
);

User.id_tipouser = User.belongsTo(Tipo_usuario, {foreignKey: 'id_tipouser', target_key: 'id'})

module.exports = User
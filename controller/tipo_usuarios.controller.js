const Tipo_usuario = require("../models/tipo_usuario");
const uuid = require("uuid");

exports.create = async (req, res) => {
  if (!req.body.nombre) {
    res.status(400).send({
      message: "nombre no puede estar vacio!",
    });
    return;
  }

  const tipo = {
    id: uuid.v4(),
    nombre: req.body.nombre,
  };

  Tipo_usuario.create(tipo)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message,
      });
    });
};

exports.findAll = (req, res) => {
  Tipo_usuario.findAll()
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message,
      });
    });
};

exports.findOne = (req, res) => {
  Tipo_usuario.findOne({ where: { id: req.params.id } })
    .then((data) => {
      if (data === null)
        res.status(500).send({
          message: err.message || "no se encontro.",
        });
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message,
      });
    });
};

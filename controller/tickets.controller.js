const Ticket = require("../models/ticket");
const User = require("../models/usuario");
const uuid = require("uuid");

exports.create = async (req, res) => {
  const user = await User.findOne({ where: { mail: req.body.mail } });

  let idUser = null;
  if (user) idUser = user.dataValues.id;

  const ticket = {
    id: uuid.v4(),
    id_user: idUser,
    ticket_pedido: false,
  };

  Ticket.create(ticket)
    .then((data) => {
      data.dataValues.usuario = user;
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message,
      });
    });
};

exports.findAll = (req, res) => {
  Ticket.findAll({
    include: [{ model: User }],
    order: [["created_at", "DESC"]],
  })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message,
      });
    });
};

exports.findAllFree = (req, res) => {
  Ticket.findAll({ where: { id_user: null } })
    .then((data) => {
      if (data === null)
        res.status(500).send({
          message: err.message || "no se encontro.",
        });
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message,
      });
    });
};

exports.findAllByUser = (req, res) => {
  const id = req.params.id;
  Ticket.findAll({ where: { id_user: id } })
    .then((data) => {
      if (data === null)
        res.status(500).send({
          message: err.message || "no se encontro.",
        });
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message,
      });
    });
};

exports.update = async (req, res) => {
  const id = req.params.id;

  if (req.body.mail) {
    const user = await User.findOne({ where: { mail: req.body.mail } });
    if (user) req.body.id_user = user.id;
    else
      res.status(500).send({
        message: "error actualizando ticket" + id,
      });
  }
  Ticket.update(req.body, {
    where: { id: id },
  })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: "ticket actualizado",
        });
      } else {
        res.send({
          message: `ticket no se pudo actualizar`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "error actualizando" + id,
      });
    });
};

exports.delete = (req, res) => {
  const id = req.params.id;

  Ticket.destroy({
    where: { id: id },
  })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: "eliminado",
        });
      } else {
        res.send({
          message: `error al eliminar`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "no se encontro ticket" + id,
      });
    });
};

const User = require("../models/usuario");
const Tipo_usuario = require("../models/tipo_usuario");
const uuid = require("uuid");
const bcrypt = require("bcrypt");
const { validationResult } = require("express-validator");
const saltRounds = 10;

//crear user
exports.create = async (req, res) => {
  const errors = validationResult(req.body);
  if (!errors.isEmpty()) {
    res.status(400).send({
      message: `Error en la data! ${JSON.stringify(errors)}`,
    });
    return;
  }

  const user = await User.findOne({ where: { mail: req.body.mail } });
  
  if (user === null)
    Tipo_usuario.findOne({
      where: { nombre: req.body.id_tipouser },
    }).then((tipo) => {
      bcrypt.hash(req.body.pass, saltRounds).then((hash) => {
        const user = {
          id: uuid.v4(),
          nombre: req.body.nombre,
          mail: req.body.mail,
          pass: hash,
          id_tipouser: tipo.getDataValue("id"),
        };

        User.create(user, { include: [{ model: Tipo_usuario }] })
          .then((data) => {
            data.dataValues.tipo_usuario = tipo;
            res.send(data);
            
          })
          .catch((err) => {
            res.status(500).send({
              message: err.message || "Error creando usuario",
            });
          });
      });
    });
    else
    res.status(400).send({
      message: `Usuario existente`,
    });
    return;
};

exports.findAll = (req, res) => {
  User.findAll({ attributes: ["mail"] })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message,
      });
    });
};

exports.findOne = async (req, res) => {
  const user = await User.findOne({
    include: [{ model: Tipo_usuario }],
    where: { mail: req.body.mail },
  });

  if (user === null)
    res.status(400).send({
      message: "Error en las credenciales",
    });

  bcrypt.compare(req.body.pass, user.pass).then((result) => {
    if (result === false) {
      res.status(400).send({
        message: `Error en las credenciales`,
      });
      return;
    }
    res.send(user);
  });
};

exports.findOneById = (req, res) => {
  const id = req.params.id;
  User.findOne({ where: { id: id } })
    .then((data) => {
      if (data === null)
        res.status(500).send({
          message: err.message || "no se encontro.",
        });
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message,
      });
    });
};

exports.findEmail = (req, res) => {
  const id = req.params.id;
  console.log(id);
  User.findOne({ where: { id: id } })
    .then((data) => {
      if (data === null)
        res.status(500).send({
          message: err.message || "no se encontro.",
        });
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message,
      });
    });
};

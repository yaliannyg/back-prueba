module.exports = (app) => {
  const tickets = require("../controller/tickets.controller");

  var router = require("express").Router();

  router.post("/", tickets.create);

  router.get("/free", tickets.findAllFree);

  router.get("/:id", tickets.findAllByUser);

  router.get("/", tickets.findAll);

  router.put("/:id", tickets.update);

  router.delete("/:id", tickets.delete);

  app.use("/api/tickets", router);
};

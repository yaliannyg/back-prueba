module.exports = (app) => {
  const tipo_usuario = require("../controller/tipo_usuarios.controller");

  var router = require("express").Router();

  router.post("/", tipo_usuario.create);
  router.get("/:id", tipo_usuario.findOne);

  router.get("/", tipo_usuario.findAll);

  app.use("/api/tipo", router);
};

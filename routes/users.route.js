module.exports = (app) => {
  const usuarios = require("../controller/usuarios.controller");
  const { check } = require("express-validator");
  var router = require("express").Router();

  router.post(
    "/",
    [
      //validar
      check("pass").isLength({ min: 6 }),
      check("nombre").isLength({ min: 6 }),
      check("mail").isEmail(),
    ],
    usuarios.create
  );
  router.post("/user", usuarios.findOne);

  router.get("/", usuarios.findAll);

  router.get("/:id", usuarios.findOneById);

  app.use("/api/users", router);
};

const { createServer } = require("http");

const express = require("express");

const bodyParser = require("body-parser");
const cors = require("cors");

const db = require("./models/index");

const PORT = 4000;

var corsOptions = {
  origin: "http://localhost:3000",
};

(async () => {
  try {
    db.sequelize.sync();
  } catch (error) {
    console.log(error);
    process.exit(0);
  }

  const app = express();
  const server = createServer(app);
  app.use(cors(corsOptions));
  app.use(bodyParser.json());

  app.use(bodyParser.urlencoded({ extended: true }));

  require("./routes/users.route")(app);
  require("./routes/tickets.route")(app);
  require("./routes/tipo_usuario.route")(app);

  server.listen(PORT);
})();

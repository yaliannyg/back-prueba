'use strict';
module.exports  = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('tipo_usuarios', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
      },
      nombre: {
        type: Sequelize.STRING,
      }
      
    }).then(()=>{
      queryInterface.addConstraint('usuarios', {
        fields: ['id_tipouser'],
        type: 'foreign key',

        references: { //Required field
          table: 'tipo_usuarios',
          field: 'id'
        },
        onDelete: 'cascade',
        onUpdate: 'cascade'
      });

    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('tipo_usuarios');
  }
};
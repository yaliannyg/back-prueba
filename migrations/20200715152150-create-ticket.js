'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('tickets', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
      },
      id_user: {
        allowNull: true,
        type: Sequelize.UUID,
      },
      ticket_pedido: {
        type: Sequelize.BOOLEAN
      },
      create_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    }).then(()=>{
      queryInterface.addConstraint('tickets', {
        fields: ['id_user'],
        type: 'foreign key',

        references: { //Required field
          table: 'usuarios',
          field: 'id'
        },
        onDelete: 'cascade',
        onUpdate: 'cascade'
      });

    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('tickets');
  }
};
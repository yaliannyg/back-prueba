"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("usuarios", {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
      },
      nombre: {
        type: Sequelize.STRING,
        
      },
      mail: {
        type: Sequelize.STRING,
      },
      pass: {
        type: Sequelize.STRING,
      },
      id_tipouser: {
        allowNull: false,
        type: Sequelize.UUID,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("usuarios");
  },
};
